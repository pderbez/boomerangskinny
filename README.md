# Boomerang Search on SKINNY

This folder contains the implementation of a boomerang search. It uses the gurobi solver to compute truncated boomerang, and the choco solver to compute the actual boomerang, and a better approximation of the probability of this boomerang.

## Dependencies

There are different dependencies for the code to compile and run. All these dependencies are jar files that should be downloaded or compiled from the sources.

For the compilation, the location of the jars is assumed, but it can be changed in the file `META-INF/manifest.txt` and Makefile.

- Jackson v2.11.0: This is the JSON parsing library. The jars needed are jackson-core, jackson-databind and jackson-annotations. They can be found in [the github page](https://github.com/FasterXML/jackson). Precisely, you can download the different jars at:
  - [jackson-core](https://github.com/FasterXML/jackson-core/releases/tag/jackson-core-2.11.0)
  - [jackson-databind](https://github.com/FasterXML/jackson-databind/releases/tag/jackson-databind-2.11.0)
  - [jackson-annotations](https://github.com/FasterXML/jackson-annotations/releases/tag/jackson-annotations-2.11.0)
- Javatuples v1.2: Useful library for dealing with tuples. The jar can be found in [the github page](https://github.com/javatuples/javatuples/downloads)
- Picocli v4.3.2: The library to have a nice CLI. The jar can be found in [the github page](https://github.com/remkop/picocli/releases/tag/v4.3.2)
- Sandwichproba v1.0.0: This needs to be compiled from the source in this repository. To do so you can go to [the sandwichproba folder](../sandwichproba), execute `make jar`, and copy the created jar `sandwichproba.jar` into your libs folder.
- Gurobi v9.0.2: The MIP solver. This solver is commercial but there are free licences for academic purposes. The jar is assumed to be in the folder `/opt/gurobi902/linux64/lib`, but that may vary depending on your installation. For all the details on how to download, install and get a licence, check their [website](https://www.gurobi.com/)
- Choco v4.10.2: The CP solver. This solver is open-source and can be downloaded from [the github page](https://github.com/chocoteam/choco-solver/releases/tag/4.10.2)

The versions are given as indications, but newer minor versions will probably work fine, as long as they are backward compatible.

## Compiling and running

You should first check in the files `META-INF/boomerangManifest.txt` and `Makefile` that the right paths of the libraries are given.
- In `META-INF/boomerangManifest.txt` you can modify the line starting with `Class-Path`
- In the `Makefile` you can change the files in the `-cp` (class path) option. Be careful to keep the `:.` at the end of the class path as it is required, no matter the location of the libraries.
Then, a simple make will compile the code.

Before running, you may want to create a folder `output/` as the default output files will be saved in this folder and if it does not exist it will raise an error. To run, the command is `java -jar boomerangsearch.jar --options`. An example can be seen with `make run`. There are different options that can be seen with the `-help` option.

## Generation of tikz

There is a script to generate a tikz representation of the boomerang differential. To compile, the only difference with the previous compilation is the manifest file `META-INF/solutionToTikzManifest.txt`, and the command `make tikz`. Then as previously a jar has been generated as `solutiontotikz.jar` that has a CLI. An example of command can be seen when doing `make runtikz`.

The output is directly done in stdout, so is has to be copy/pasted in your latex project by hand. Only the tikzpicture is generated, without the headers and imports for the compilation. When generating a step 1 tikz representation, the jar will create the representation with the grid and the colors. When generating a step 2 representation, the jar will only write the values of the cells, so the output has to be added to an output of a step 1 to actually understand what is the cell.

## Explanation for files and directories

- `step1/`, `step2/` and `solutiontotikz/` contain all the classes for models and generation of tikz representation.
- Compilation and running files (does not include output), can be cleaned by `make clean`:
  - `boomerangsearch/` is a folder created by the compilation.
  - `boomerangsearch.jar` is the runnable jar for the models.
  - `solutiontotikz.jar` is the runnable jar for the generation of a tikz representation.
  - `model.mps` is an export of the model. Can be used to do auto-tuning of parameters.
- `META-INF/` is the folder declaring the manifest files, defining the dependencies for the construction of the jars
- `tune1.prm` is a set of parameters that will be used by Gurobi during the first step of the search.
