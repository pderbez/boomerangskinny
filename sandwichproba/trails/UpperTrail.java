package sandwichproba.trails;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

/**
 * Represents an upper trail of multiple rounds of a skinny cipher
 * 
 * @author Mathieu Vavrille
 */
public class UpperTrail {
  private final int nbRounds;
  private final int[][] inputDifference;
  private final int[][][] keyDifferences;
  private Variable[][][] sboxStates; // The sboxStates are the output states of the sboxes
  private Map<Variable,Integer> affectation;

  /**
   * @param nbRounds the number of rounds on which to do the trail, not considering the linear part on the last round
   * @param inputDifference the input difference at the beginning of the trail
   * @param keyDifferences the differences in the key for each round considered (except the last one)
   */
  public UpperTrail(final int nbRounds, final int[][] inputDifference, final int[][][] keyDifferences) {
    this.nbRounds = nbRounds;
    this.inputDifference = inputDifference;
    this.keyDifferences = keyDifferences;
    affectation = new HashMap<Variable,Integer>();
    sboxStates = new Variable[nbRounds][4][4];
    // propagate the zeros through the rounds
    for (int round = 0; round < nbRounds; round++) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          final Variable currentVar = new Variable(round, i, j);
          sboxStates[round][i][j] = currentVar;
          if (getLinearExpressionBeforeSbox(currentVar).isZero())
            affectation.put(currentVar, 0);
        }
      }
    }
  }

  /**
   * Propagates the linear part of the cipher
   * @param var a variable, ie an output of a sbox
   * @return The expression input of the sbox of the variable, considering the variables of the previous round
   */
  public XorExpr getLinearExpressionBeforeSbox(final Variable var) {
    final int round = var.getPosRound();
    final int i = var.getPosI();
    final int j = var.getPosJ();
    if (round == 0) {
      return XorExpr.ofConstant(inputDifference[i][j]);
    }
    XorExpr retExpr = new XorExpr();
    switch (i) {
    case 0:
      retExpr = XorExpr.ofConstant(keyDifferences[round-1][0][j]).add(sboxStates[round-1][3][(j+1)%4]).add(sboxStates[round-1][0][j]).add(sboxStates[round-1][2][(j+2)%4]);
      break;
    case 1:
      retExpr = XorExpr.ofConstant(keyDifferences[round-1][0][j]).add(sboxStates[round-1][0][j]);
      break;
    case 2:
      retExpr = XorExpr.ofConstant(keyDifferences[round-1][1][(j+3)%4]).add(sboxStates[round-1][1][(j+3)%4]).add(sboxStates[round-1][2][(j+2)%4]);
      break;
    case 3:
      retExpr = XorExpr.ofConstant(keyDifferences[round-1][0][j]).add(sboxStates[round-1][0][j]).add(sboxStates[round-1][2][(j+2)%4]);
      break;
    default:
      throw new IllegalStateException("i should be 0, 1, 2 or 3");
    }
    return retExpr.evaluate(affectation);
  }

  /**
   * @param initialVariable
   * @return all the variables that can influence this variable
   */
  public Set<Variable> getVariablesRequiredBy(final Variable initialVariable) {
    //
    final Set<Variable> requiredVariables = new HashSet<Variable>();
    if (initialVariable.getPosRound() > 0) {
      getLinearExpressionBeforeSbox(initialVariable).getVariables().stream()
        .forEach(var -> {requiredVariables.addAll(getVariablesRequiredBy(var)); requiredVariables.add(var);});
    }
    return requiredVariables;
  }

  public Variable getSboxState(final int round, final int i, final int j) {
    return sboxStates[round][i][j];
  }

  public XorExpr getSboxExpr(final int round, final int i, final int j) {
    return XorExpr.ofVariable(getSboxState(round, i, j)).evaluate(affectation);
  }

  /**
   * @return all the variables equal to zero, given the input difference
   */
  public Set<Variable> getZeroVariables() {
    return affectation.keySet();
  }




    
}
