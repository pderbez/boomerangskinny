package sandwichproba.trails;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtransition.Formula;
import sandwichproba.sboxtransition.SboxTransition;
import sandwichproba.sboxtransition.FBCTTransition;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.Collectors;


/**
 * This class does the computation to generate a formula helping to compute the probability of the boomerang
 *
 * @author Mathieu Vavrille
 */
public class FormulaGeneration {
  private final int nbRounds;
  private final int[][] inputDifference;
  private final int[][][] upperKeyDifference;
  private final int[][] outputDifference;
  private final int[][][] lowerKeyDifference;
  private final UpperTrail upperTrail;
  private final LowerTrail lowerTrail;

  public FormulaGeneration(final int nbRounds,
                           final int[][] inputDifference, final int[][][] upperKeyDifference,
                           final int[][] outputDifference, final int[][][] lowerKeyDifference) {
    this.nbRounds = nbRounds;
    this.inputDifference = inputDifference;
    this.upperKeyDifference = upperKeyDifference;
    this.outputDifference = outputDifference;
    this.lowerKeyDifference = lowerKeyDifference;
    
    upperTrail = new UpperTrail(nbRounds, inputDifference, upperKeyDifference);
    lowerTrail = new LowerTrail(nbRounds, outputDifference, lowerKeyDifference);

  }

  /**
   * Get all the intersting variables, the ones that should appear in the formula
   * @param zeroVariables a set of variables equal to zero
   */
  private Set<Variable> getInterestingVariables(final Set<Variable> zeroVariables) {
    final Set<Variable> interestingVariables = new HashSet<Variable>();
    for (int round = 0; round < nbRounds; round ++) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          if (!zeroVariables.contains(lowerTrail.getSboxState(round, i, j))) {
            interestingVariables.addAll(upperTrail.getVariablesRequiredBy(upperTrail.getSboxState(round, i, j)));
          }
          if (!zeroVariables.contains(upperTrail.getSboxState(round, i, j))) {
            interestingVariables.addAll(lowerTrail.getVariablesRequiredBy(lowerTrail.getSboxState(round, i, j)));
          }
        }
      }
    }
    return interestingVariables;
  }

  /**
   * @return a formula to compute the probability of the boomerang
   */
  public Formula getFormula() {
    final Set<Variable> zeroVariables = new HashSet<Variable>(upperTrail.getZeroVariables());
    zeroVariables.addAll(lowerTrail.getZeroVariables());
    final Set<Variable> interestingVariables = getInterestingVariables(zeroVariables);
    
    List<SboxTransition> formula = new ArrayList<SboxTransition>();
    for (int round = 0; round < nbRounds; round ++) {
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          formula.add(FBCTTransition.transitionOf(upperTrail.getLinearExpressionBeforeSbox(upperTrail.getSboxState(round, i, j)),
                                                  (interestingVariables.contains(upperTrail.getSboxState(round, i, j))) ? upperTrail.getSboxExpr(round, i, j) : null,
                                                  (interestingVariables.contains(lowerTrail.getSboxState(round, i, j))) ? lowerTrail.getSboxExpr(round, i, j) : null,
                                                  lowerTrail.getLinearExpressionAfterSbox(lowerTrail.getSboxState(round, i, j))));
        }
      }
    }
    return new Formula(formula.stream()
                       .filter(SboxTransition::isInteresting)
                       .collect(Collectors.toList()));
  }
  
}
