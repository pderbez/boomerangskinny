package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtables.SboxTables;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

/**
 * The UBCT transiton in a Formula
 *
 * @author Mathieu Vavrille
 */
public class UBCTTransition extends SboxTransition {

  private final XorExpr gamma;
  private final XorExpr theta;
  private final XorExpr delta;

  public UBCTTransition(final XorExpr gamma, final XorExpr theta, final XorExpr delta) {
    this.gamma = gamma;
    this.theta = theta;
    this.delta = delta;
  }

  public static SboxTransition transitionOf(final XorExpr gamma, final XorExpr theta, final XorExpr delta) {
    if (theta == null)
      return new BCTTransition(gamma, delta);
    if (delta.isZero())
      return new DDTTransition(gamma, theta);
    return new UBCTTransition(gamma, theta, delta);
  }
  
  public Set<Variable> getVariables() {
    final Set<Variable> variables = new HashSet<Variable>();
    Arrays.asList(gamma, theta, delta).stream()
      .forEach(expr -> variables.addAll(expr.getVariables()));
    return variables;
  }

  public String toString() {
    return "UBCT("+ gamma + "," + theta + "," + delta + ")";
  }

  public boolean isInteresting() {
    return true;
  }

  public UBCTTransition imposeVariable(final Variable variable, final int value) {
    return new UBCTTransition(gamma.imposeVariable(variable, value),
                              theta.imposeVariable(variable, value),
                              delta.imposeVariable(variable, value));
  }

  public double getCstProba(final SboxTables sboxTables) {
    return sboxTables.ubctProba(gamma.toConstant(), theta.toConstant(), delta.toConstant());
  }

  public Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables) {
    if (gamma.isConstant() && theta.getVariables().contains(variable) && theta.getVariables().size() == 1) {
      final Set<Integer> possibleThetas = sboxTables.getPossibleOutputDiffsDDT(gamma.getConstant());
      if (theta.getConstant() == 0)
        return possibleThetas;
      else {
        final int cst = theta.getConstant();
        return possibleThetas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (theta.isConstant() && gamma.getVariables().contains(variable) && gamma.getVariables().size() == 1) {
      final Set<Integer> possibleGammas = sboxTables.getPossibleInputDiffsDDT(theta.getConstant());
      if (gamma.getConstant() == 0)
        return possibleGammas;
      else {
        final int cst = gamma.getConstant();
        return possibleGammas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    return IntStream.range(0,sboxTables.getSboxSize())
      .boxed()
      .collect(Collectors.toSet());
  }
}
