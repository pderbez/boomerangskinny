package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtables.SboxTables;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

/**
 * The BCT transiton in a Formula
 *
 * @author Mathieu Vavrille
 */
public class BCTTransition extends SboxTransition {

  private final XorExpr gamma;
  private final XorExpr delta;

  public BCTTransition(final XorExpr gamma, final XorExpr delta) {
    this.gamma = gamma;
    this.delta = delta;
  }

  public Set<Variable> getVariables() {
    final Set<Variable> variables = new HashSet<Variable>();
    Arrays.asList(gamma, delta).stream()
      .forEach(expr -> variables.addAll(expr.getVariables()));
    return variables;
  }

  public String toString() {
    return "BCT("+ gamma + "," + delta + ")";
  }

  public boolean isInteresting() {
    return !gamma.isZero() && !delta.isZero();
  }

  public BCTTransition imposeVariable(final Variable variable, final int value) {
    return new BCTTransition(gamma.imposeVariable(variable, value), delta.imposeVariable(variable, value));
  }

  public double getCstProba(final SboxTables sboxTables) {
    return sboxTables.bctProba(gamma.toConstant(), delta.toConstant());
  }

  public Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables) {
    if (gamma.isConstant() && delta.getVariables().contains(variable) && delta.getVariables().size() == 1) {
      final Set<Integer> possibleDeltas = sboxTables.getPossibleOutputDiffsBCT(gamma.getConstant());
      if (delta.getConstant() == 0)
        return possibleDeltas;
      else {
        final int cst = delta.getConstant();
        return possibleDeltas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (delta.isConstant() && gamma.getVariables().contains(variable) && gamma.getVariables().size() == 1) {
      final Set<Integer> possibleGammas = sboxTables.getPossibleInputDiffsBCT(delta.getConstant());
      if (gamma.getConstant() == 0)
        return possibleGammas;
      else {
        final int cst = gamma.getConstant();
        return possibleGammas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    return IntStream.range(0,sboxTables.getSboxSize())
      .boxed()
      .collect(Collectors.toSet());
  }
}
