package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtables.SboxTables;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

/**
 * The DDT transiton in a Formula
 *
 * @author Mathieu Vavrille
 */
public class DDTTransition extends SboxTransition {

  private final XorExpr alpha;
  private final XorExpr beta;

  public DDTTransition(final XorExpr alpha, final XorExpr beta) {
    this.alpha = alpha;
    this.beta = beta;
  }

  public Set<Variable> getVariables() {
    final Set<Variable> variables = new HashSet<Variable>();
    Arrays.asList(alpha, beta).stream()
      .forEach(expr -> variables.addAll(expr.getVariables()));
    return variables;
  }

  public String toString() {
    return "DDT("+ alpha + "," + beta + ")";
  }

  public boolean isInteresting() {
    return true;
  }

  public DDTTransition imposeVariable(final Variable variable, final int value) {
    return new DDTTransition(alpha.imposeVariable(variable, value), beta.imposeVariable(variable, value));
  }

  public double getCstProba(final SboxTables sboxTables) {
    return sboxTables.ddtProba(alpha.toConstant(), beta.toConstant());
  }

  public Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables) {
    if (alpha.isConstant() && beta.getVariables().contains(variable) && beta.getVariables().size() == 1) {
      final Set<Integer> possibleBetas = sboxTables.getPossibleOutputDiffsDDT(alpha.getConstant());
      if (beta.getConstant() == 0)
        return possibleBetas;
      else {
        final int cst = beta.getConstant();
        return possibleBetas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (beta.isConstant() && alpha.getVariables().contains(variable) && alpha.getVariables().size() == 1) {
      final Set<Integer> possibleAlphas = sboxTables.getPossibleInputDiffsDDT(beta.getConstant());
      if (alpha.getConstant() == 0)
        return possibleAlphas;
      else {
        final int cst = alpha.getConstant();
        return possibleAlphas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    return IntStream.range(0,sboxTables.getSboxSize())
      .boxed()
      .collect(Collectors.toSet());
  }
}
