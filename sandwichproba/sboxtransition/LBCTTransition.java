package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtables.SboxTables;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

/**
 * The LBCT transiton in a Formula
 *
 * @author Mathieu Vavrille
 */
public class LBCTTransition extends SboxTransition {

  private final XorExpr gamma;
  private final XorExpr lambda;
  private final XorExpr delta;

  public LBCTTransition(final XorExpr gamma, final XorExpr lambda, final XorExpr delta) {
    this.gamma = gamma;
    this.lambda = lambda;
    this.delta = delta;
  }

  public static SboxTransition transitionOf(final XorExpr gamma, final XorExpr lambda, final XorExpr delta) {
    if (lambda == null)
      return new BCTTransition(gamma, delta);
    if (gamma.isZero())
      return new DDTTransition(lambda, delta);
    return new LBCTTransition(gamma, lambda, delta);
  }

  public Set<Variable> getVariables() {
    final Set<Variable> variables = new HashSet<Variable>();
    Arrays.asList(gamma, lambda, delta).stream()
      .forEach(expr -> variables.addAll(expr.getVariables()));
    return variables;
  }

  public String toString() {
    return "LBCT("+ gamma + "," + lambda + "," + delta + ")";
  }

  public boolean isInteresting() {
    return true;
  }

  public LBCTTransition imposeVariable(final Variable variable, final int value) {
    return new LBCTTransition(gamma.imposeVariable(variable, value),
                              lambda.imposeVariable(variable, value),
                              delta.imposeVariable(variable, value));
  }

  public double getCstProba(final SboxTables sboxTables) {
    return sboxTables.lbctProba(gamma.toConstant(), lambda.toConstant(), delta.toConstant());
  }

  public Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables) {
    if (lambda.isConstant() && delta.getVariables().contains(variable) && delta.getVariables().size() == 1) {
      final Set<Integer> possibleDeltas = sboxTables.getPossibleOutputDiffsDDT(lambda.getConstant());
      if (delta.getConstant() == 0)
        return possibleDeltas;
      else {
        final int cst = delta.getConstant();
        return possibleDeltas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (delta.isConstant() && lambda.getVariables().contains(variable) && lambda.getVariables().size() == 1) {
      final Set<Integer> possibleLambdas = sboxTables.getPossibleInputDiffsDDT(delta.getConstant());
      if (lambda.getConstant() == 0) {
        return possibleLambdas;
      }
      else {
        final int cst = lambda.getConstant();
        return possibleLambdas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    return IntStream.range(0,sboxTables.getSboxSize())
      .boxed()
      .collect(Collectors.toSet());
  }
}
