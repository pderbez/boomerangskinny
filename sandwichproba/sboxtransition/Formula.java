package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.TreeVariable;
import sandwichproba.util.Graph;
import sandwichproba.sboxtables.SboxTables;

import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import org.javatuples.Pair;
import java.util.concurrent.TimeUnit;

/**
 * The representation of a Formula as a list of transitions.
 * This class helps to compute the value of the formula by summing variables.
 *
 * @author Mathieu Vavrille
 */
public class Formula {
  private List<SboxTransition> sboxTransitions;
  private static final int printDepth = 2;

  public Formula(final List<SboxTransition> sboxTransitions) {
    this.sboxTransitions = sboxTransitions;
  }

  public Formula imposeVariable(final Variable variable, final int value) {
    return new Formula(sboxTransitions.stream()
                       .map(transition -> transition.imposeVariable(variable, value))
                       .collect(Collectors.toList()));
  }

  private Set<Integer> transitionIdOf(final Variable variable, final Set<Variable> visited, final List<SboxTransition> transitions) {
    if (visited.contains(variable))
      return new HashSet<Integer>();
    visited.add(variable);
    final Set<Integer> transitionIds = new HashSet<Integer>();
    for (int i = 0; i < transitions.size(); i++) {
      Set<Variable> currentVariableSet = transitions.get(i).getVariables();
      if (currentVariableSet.contains(variable)) {
        transitionIds.add(i);
        currentVariableSet.stream()
          .forEach(newVar -> transitionIds.addAll(transitionIdOf(newVar, visited, transitions)));
      }
    }
    return transitionIds;
  }

  private List<Formula> split() {
    final List<Formula> splitted = new ArrayList<Formula>();
    final List<SboxTransition> constantTransitions = sboxTransitions.stream().filter(tr -> tr.getVariables().size() == 0).collect(Collectors.toList());
    if (constantTransitions.size() != 0)
      splitted.add(new Formula(constantTransitions));
    final Set<Variable> visited = new HashSet<Variable>();
    final List<Variable> allVars = new ArrayList<Variable>(getVariables());
    for (int i = 0; i < allVars.size(); i++) {
      if (!visited.contains(allVars.get(i)))
        splitted.add(new Formula(transitionIdOf(allVars.get(i), visited, sboxTransitions).stream().map(id -> sboxTransitions.get(id)).collect(Collectors.toList())));
    }
    /*if (splitted.size() > 2 || constantTransitions.size() == 0 && splitted.size() > 1) {
      //System.out.println("division");
      //System.out.println(toString());
      //System.out.println(splitted);
      }*/
    return splitted;
  }
                                                                            

  public Set<Variable> getVariables() {
    return sboxTransitions.stream()
      .flatMap(transition -> transition.getVariables().stream())
      .collect(Collectors.toSet());
  }

  public String toString() {
    return String.join(" * ", sboxTransitions.stream().map(transition -> transition.toString()).collect(Collectors.toList()));
  }

  public double evaluate(final SboxTables sboxTables) {
    System.out.println("Creation of tree");
    Graph g = Graph.ofHypergraph(sboxTransitions.stream().map(transition -> transition.getVariables()).collect(Collectors.toList()));
    TreeVariable affectationTree = g.getAffectationTree();
    affectationTree.preetyPrint();
    System.out.println("Tree created");
    return evaluateTree(sboxTables, affectationTree, 0);
  }

  public double evaluateTree(final SboxTables sboxTables, final TreeVariable affectationTree, final int depth) {
    if (!affectationTree.hasLevelVariables()) {
      if (affectationTree.isLeaf()) {
        return sboxTransitions.stream()
          .reduce(1., (acc, currentTransition) -> (acc == 0.) ? 0. : acc*currentTransition.getCstProba(sboxTables), (s1, s2) -> { throw new IllegalStateException("I am not using parallel computations"); });
      }
      else {
        List<Pair<Formula, TreeVariable>> splittedFormulaAndTree = splitWithForest(affectationTree.getSubTrees());
        /*double result = 1.;
        for (int i = 0; result != 0. && i < splittedFormulaAndTree.size(); i++) {
          result = result*splittedFormulaAndTree.get(i).getValue0().evaluateTree(sboxTables,splittedFormulaAndTree.get(i).getValue1());
        }
        return result;*/
        return splittedFormulaAndTree.stream()
          .reduce(1., (acc, formulaAndTree) -> (acc == 0.) ? 0. : acc*formulaAndTree.getValue0().evaluateTree(sboxTables,formulaAndTree.getValue1(), depth +1), (s1, s2) -> { throw new IllegalStateException("I am not using parallel computations"); });
      }
    }
    else {
      final Pair<Variable, Set<Integer>> variableAndDomain = getVariableAndDomainToInstanciate(sboxTables, affectationTree.getLevelVariables());
      final Variable currentVariable = variableAndDomain.getValue0();
      final Set<Integer> currentVariableDomain = variableAndDomain.getValue1();
      affectationTree.removeLevelVariable(currentVariable);
      if (depth < printDepth)
        System.out.println(currentVariable + " " + currentVariableDomain);
      final double sumOfThisVariable = currentVariableDomain.stream()
        .mapToDouble(value -> imposeVariable(currentVariable, value).evaluateTree(sboxTables, affectationTree, depth+1))
        .sum();
      affectationTree.addLevelVariable(currentVariable);
      return sumOfThisVariable;
    }
  }

  private List<Pair<Formula, TreeVariable>> splitWithForest(final List<TreeVariable> forest) {
    final List<SboxTransition> constantTransitions = sboxTransitions.stream()
      .filter(transition -> transition.getVariables().isEmpty())
      .collect(Collectors.toList());
    final List<Pair<Formula, TreeVariable>> splitted = new ArrayList<Pair<Formula, TreeVariable>>();
    if (!constantTransitions.isEmpty())
      splitted.add(new Pair<Formula,TreeVariable>(new Formula(constantTransitions), new TreeVariable()));
    splitted.addAll(forest.stream()
                    .map(tree -> new Pair<Formula,TreeVariable>(new Formula(sboxTransitions.stream().filter(transition -> !constantTransitions.contains(transition) && tree.getVariables().containsAll(transition.getVariables())).collect(Collectors.toList())),tree))
                    .collect(Collectors.toList()));
    return splitted;
  }

  private Pair<Variable, Set<Integer>> getVariableAndDomainToInstanciate(final SboxTables sboxTables, final Set<Variable> variables) {
    final Map<Variable, Set<Integer>> variableAndDomains = new HashMap<Variable, Set<Integer>>();
    variables.stream()
      .forEach(var -> variableAndDomains.put(var, getVariableDomain(var, sboxTables)));
    final Variable smallestDomainVariable = variables.stream()
      .map(var -> new Pair<Integer, Variable>(variableAndDomains.get(var).size(), var))
      .sorted()
      .map(pair -> pair.getValue1())
      .collect(Collectors.toList())
      .get(0);
    return new Pair<Variable, Set<Integer>>(smallestDomainVariable, variableAndDomains.get(smallestDomainVariable));
  }

  private Set<Integer> getVariableDomain(final Variable var, final SboxTables sboxTables) {
    final Set<Integer> domain = IntStream.range(0,sboxTables.getSboxSize()).boxed().collect(Collectors.toSet());;
    sboxTransitions.stream().forEach(transition -> domain.retainAll(transition.getVariableDomain(var, sboxTables)));
    return domain;
  }
}


/*
UBCT(4,V9-0-2-1,V135-1-1-3) * DDT(V9-0-2-1,V19-1-0-3) * DDT(V135-1-1-3,V124-2-3-0) * DDT(V9-0-2-1,V27-1-2-3) * DDT(V9-0-2-1,V31-1-3-3) * DDT(V31-1-3-3,V34-2-0-2) * DDT(V19-1-0-3,V35-2-0-3) * DDT(V19-1-0-3,V39-2-1-3) * UBCT(V27-1-2-3,V41-2-2-1,V111-3-3-3) * DDT(V124-2-3-0,V111-3-3-3) * DDT(V19-1-0-3,V47-2-3-3) * DDT(V47-2-3-3+V34-2-0-2,V50-3-0-2) * DDT(V39-2-1-3,V56-3-2-0) * FBCT(V35-2-0-3+V41-2-2-1,V63-3-3-3,V111-3-3-3,V82-4-0-2) * LBCT(V63-3-3-3+V50-3-0-2+V56-3-2-0,V82-4-0-2,10)

 */
