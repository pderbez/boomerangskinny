package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.util.XorExpr;
import sandwichproba.sboxtables.SboxTables;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

/**
 * The FBCT transiton in a Formula
 *
 * @author Mathieu Vavrille
 */
public class FBCTTransition extends SboxTransition {

  private final XorExpr gamma;
  private final XorExpr theta;
  private final XorExpr lambda;
  private final XorExpr delta;

  public FBCTTransition(final XorExpr gamma, final XorExpr theta, final XorExpr lambda, final XorExpr delta) {
    this.gamma = gamma;
    this.theta = theta;
    this.lambda = lambda;
    this.delta = delta;
  }
  
  public static SboxTransition transitionOf(final XorExpr gamma, final XorExpr theta, final XorExpr lambda, final XorExpr delta) {
    if (theta == null)
      return LBCTTransition.transitionOf(gamma, lambda, delta);
    if (lambda == null)
      return UBCTTransition.transitionOf(gamma, theta, delta);
    if (gamma.isZero() && theta.isZero())
      return new DDTTransition(lambda, delta);
    if (lambda.isZero() && delta.isZero())
      return new DDTTransition(gamma, theta);
    if (gamma.isZero() || theta.isZero() || lambda.isZero() || delta.isZero())
      throw new IllegalStateException("There has been a problem in the propagation of zeros through Sboxes");
    return new FBCTTransition(gamma, theta, lambda, delta);
  }

  public Set<Variable> getVariables() {
    final Set<Variable> variables = new HashSet<Variable>();
    Arrays.asList(gamma, theta, lambda, delta).stream()
      .forEach(expr -> variables.addAll(expr.getVariables()));
    return variables;
  }

  public String toString() {
    return "FBCT("+ gamma + "," + theta + "," + lambda + "," + delta + ")";
  }

  public boolean isInteresting() {
    return true;
  }

  public FBCTTransition imposeVariable(final Variable variable, final int value) {
    return new FBCTTransition(gamma.imposeVariable(variable, value),
                              theta.imposeVariable(variable, value),
                              lambda.imposeVariable(variable, value),
                              delta.imposeVariable(variable, value));
  }

  public double getCstProba(final SboxTables sboxTables) {
    return sboxTables.fbctProba(gamma.toConstant(), theta.toConstant(), lambda.toConstant(), delta.toConstant());
  }

  public Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables) {
    if (gamma.isConstant() && theta.getVariables().contains(variable) && theta.getVariables().size() == 1) {
      final Set<Integer> possibleThetas = sboxTables.getPossibleOutputDiffsDDT(gamma.getConstant());
      if (theta.getConstant() == 0)
        return possibleThetas;
      else {
        final int cst = theta.getConstant();
        return possibleThetas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (theta.isConstant() && gamma.getVariables().contains(variable) && gamma.getVariables().size() == 1) {
      final Set<Integer> possibleGammas = sboxTables.getPossibleInputDiffsDDT(theta.getConstant());
      if (gamma.getConstant() == 0)
        return possibleGammas;
      else {
        final int cst = gamma.getConstant();
        return possibleGammas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (lambda.isConstant() && delta.getVariables().contains(variable) && delta.getVariables().size() == 1) {
      final Set<Integer> possibleDeltas = sboxTables.getPossibleOutputDiffsDDT(lambda.getConstant());
      if (delta.getConstant() == 0)
        return possibleDeltas;
      else {
        final int cst = delta.getConstant();
        return possibleDeltas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    if (delta.isConstant() && lambda.getVariables().contains(variable) && lambda.getVariables().size() == 1) {
      final Set<Integer> possibleLambdas = sboxTables.getPossibleInputDiffsDDT(delta.getConstant());
      if (lambda.getConstant() == 0)
        return possibleLambdas;
      else {
        final int cst = lambda.getConstant();
        return possibleLambdas.stream().map(i -> i^cst).collect(Collectors.toSet());
      }
    }
    return IntStream.range(0,sboxTables.getSboxSize())
      .boxed()
      .collect(Collectors.toSet());
  }

}
