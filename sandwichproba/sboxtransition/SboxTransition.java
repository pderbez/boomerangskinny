package sandwichproba.sboxtransition;

import sandwichproba.util.Variable;
import sandwichproba.sboxtables.SboxTables;

import java.util.Set;

/**
 * Abstract class to represent the transitions in the formula
 *
 * @author Mathieu Vavrille
 */
public abstract class SboxTransition {

  public abstract Set<Variable> getVariables();

  public abstract boolean isInteresting();

  public abstract SboxTransition imposeVariable(final Variable variable, final int value);

  public abstract String toString();

  public abstract double getCstProba(final SboxTables sboxTables);

  public abstract Set<Integer> getVariableDomain(final Variable variable, final SboxTables sboxTables);
}
