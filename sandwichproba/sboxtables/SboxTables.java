package sandwichproba.sboxtables;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.javatuples.Triplet;
import org.javatuples.Quartet;

/**
 * This class allows to compute the transition probabilities of ddt, bct, Ubct, Lbct and Fbct.
 * It uses memoization to speed-up the computations.
 *
 * @author Mathieu Vavrille
 */
public class SboxTables {
  private final Sbox sbox;
  private final int[][] ddtMemo;
  private final int[][] bctMemo;
  private final Map<Triplet<Integer,Integer,Integer>,Integer> ubctMemo;
  private final Map<Triplet<Integer,Integer,Integer>,Integer> lbctMemo;
  private final Map<Quartet<Integer,Integer,Integer,Integer>,Integer> fbctMemo;
  private final Map<Integer, Set<Integer>> possibleOutputDiffsDDTMemo;
  private final Map<Integer, Set<Integer>> possibleInputDiffsDDTMemo;
  private final Map<Integer, Set<Integer>> possibleOutputDiffsBCTMemo;
  private final Map<Integer, Set<Integer>> possibleInputDiffsBCTMemo;

  public SboxTables(final Sbox sbox) {
    this.sbox = sbox;
    ddtMemo = generateDDT();
    final Quartet<
      int[][],
      Map<Triplet<Integer,Integer,Integer>,Integer>,
      Map<Triplet<Integer,Integer,Integer>,Integer>,
      Map<Quartet<Integer,Integer,Integer,Integer>,Integer>> allBCTs = generateBCTs();
    bctMemo = allBCTs.getValue0();
    ubctMemo = allBCTs.getValue1();
    lbctMemo = allBCTs.getValue2();
    fbctMemo = allBCTs.getValue3();
    possibleOutputDiffsDDTMemo = IntStream.range(0,sbox.size()).boxed()
      .collect(Collectors.toMap(input -> input,
                                input -> IntStream.range(0,sbox.size()).boxed()
                               .filter(output -> ddtMemo[input][output] != 0)
                               .collect(Collectors.toSet())));
    possibleInputDiffsDDTMemo = IntStream.range(0,sbox.size()).boxed()
      .collect(Collectors.toMap(output -> output,
                               output -> IntStream.range(0,sbox.size()).boxed()
                               .filter(input -> ddtMemo[input][output] != 0)
                               .collect(Collectors.toSet())));
    possibleOutputDiffsBCTMemo = IntStream.range(0,sbox.size()).boxed()
      .collect(Collectors.toMap(input -> input,
                               input -> IntStream.range(0,sbox.size()).boxed()
                               .filter(output -> bctMemo[input][output] != 0)
                               .collect(Collectors.toSet())));
    possibleInputDiffsBCTMemo = IntStream.range(0,sbox.size()).boxed()
      .collect(Collectors.toMap(output -> output,
                               output -> IntStream.range(0,sbox.size()).boxed()
                               .filter(input -> bctMemo[input][output] != 0)
                               .collect(Collectors.toSet())));
  }

  /** Computes the DDT */
  private int[][] generateDDT() {
    final int[][] ddtEmpty = new int[sbox.size()][sbox.size()];
    for (int x = 0; x < sbox.size(); x++) {
      for (int alpha = 0; alpha < sbox.size(); alpha++) {
        ddtEmpty[alpha][sbox.get(x)^sbox.get(x^alpha)]++;
      }
    }
    return ddtEmpty;
  }

  /** Computes the BCT tables: bct, Ubct, Lbct, Fbct */
  private Quartet<int[][],
    Map<Triplet<Integer,Integer,Integer>,Integer>,
    Map<Triplet<Integer,Integer,Integer>,Integer>,
    Map<Quartet<Integer,Integer,Integer,Integer>,Integer>> generateBCTs() {
    final int[][] bctEmpty = new int[sbox.size()][sbox.size()];
    final Map<Triplet<Integer,Integer,Integer>,Integer> ubctEmpty = new HashMap<Triplet<Integer,Integer,Integer>,Integer>();
    final Map<Triplet<Integer,Integer,Integer>,Integer> lbctEmpty = new HashMap<Triplet<Integer,Integer,Integer>,Integer>();
    final Map<Quartet<Integer,Integer,Integer,Integer>,Integer> fbctEmpty = new HashMap<Quartet<Integer,Integer,Integer,Integer>,Integer>();
    for (int x = 0; x < sbox.size(); x++) {
      for (int gamma = 0; gamma < sbox.size(); gamma++) {
        for (int delta = 0; delta < sbox.size(); delta++) {
          if ((sbox.getInv(sbox.get(x)^delta)^sbox.getInv(sbox.get(x^gamma)^delta)) == gamma) {
            bctEmpty[gamma][delta]++;
            final int theta = sbox.get(x)^sbox.get(x^gamma);
            final int lambda = x^sbox.getInv(sbox.get(x)^delta);
            increaseEntryByOne(ubctEmpty, new Triplet<Integer,Integer,Integer>(gamma, theta, delta));
            increaseEntryByOne(lbctEmpty, new Triplet<Integer,Integer,Integer>(gamma, lambda, delta));
            increaseEntryByOne(fbctEmpty, new Quartet<Integer,Integer,Integer,Integer>(gamma, theta, lambda, delta));
          }
        }
      }
    }
    return new Quartet<int[][],
    Map<Triplet<Integer,Integer,Integer>,Integer>,
    Map<Triplet<Integer,Integer,Integer>,Integer>,
    Map<Quartet<Integer,Integer,Integer,Integer>,Integer>>(bctEmpty, ubctEmpty, lbctEmpty, fbctEmpty);
  }

  /** Helper to increase the entry of a map by one if it exists, or put one in the entry */
  private <T> void increaseEntryByOne(final Map<T, Integer> map, final T key) {
    map.put(key, getEntryOrZero(map, key)+1);
  }

  /** Get the value associated to a key in the map, or zero if the value does not exist */
  private <T> int getEntryOrZero(final Map<T, Integer> map, final T key) {
    if (map.containsKey(key))
      return map.get(key);
    else
      return 0;
  }

  // ------ Computation of the probabilities using the memoized tables ------
  public int ddt(final int gamma, final int delta) {
    return ddtMemo[gamma][delta];
  }
  
  public int bct(final int gamma, final int delta) {
    return bctMemo[gamma][delta];
  }
  
  public double ddtProba(final int gamma, final int delta) {
    return (double) ddtMemo[gamma][delta] / sbox.size();
  }

  public double bctProba(final int gamma, final int delta) {
    return (double) bctMemo[gamma][delta] / sbox.size();
  }

  public double ubctProba(final int gamma, final int theta, final int delta) {
    return ((double) getEntryOrZero(ubctMemo, new Triplet<Integer,Integer,Integer>(gamma, theta, delta))) / sbox.size();
  }

  public double lbctProba(final int gamma, final int lambda, final int delta) {
    return ((double) getEntryOrZero(lbctMemo, new Triplet<Integer,Integer,Integer>(gamma, lambda, delta))) / sbox.size();
  }

  public double fbctProba(final int gamma, final int theta, final int lambda, final int delta) {
    return (double) getEntryOrZero(fbctMemo, new Quartet<Integer,Integer,Integer,Integer>(gamma, theta, lambda, delta)) / sbox.size();
  }

  public Set<Map.Entry<Triplet<Integer,Integer,Integer>,Integer>> getUBCTEntrySet() {
    return ubctMemo.entrySet();
  }

  public Set<Map.Entry<Triplet<Integer,Integer,Integer>,Integer>> getLBCTEntrySet() {
    return lbctMemo.entrySet();
  }

  public Set<Map.Entry<Quartet<Integer,Integer,Integer,Integer>,Integer>> getFBCTEntrySet() {
    return fbctMemo.entrySet();
  }

  // ------ Functions to get the possible sets of values in input or output of the transitions ddt or bct ------
  
  public Set<Integer> getPossibleOutputDiffsDDT(final int inputDiff) {
    return possibleOutputDiffsDDTMemo.get(inputDiff);
  }

  public Set<Integer> getPossibleInputDiffsDDT(final int outputDiff) {
    return possibleInputDiffsDDTMemo.get(outputDiff);
  }

  public Set<Integer> getPossibleOutputDiffsBCT(final int inputDiff) {
    return possibleOutputDiffsBCTMemo.get(inputDiff);
  }

  public Set<Integer> getPossibleInputDiffsBCT(final int outputDiff) {
    return possibleInputDiffsBCTMemo.get(outputDiff);
  }

  public int getSboxSize() {
    return sbox.size();
  }

  private int sumValues(Map<Integer,Integer> map) {
    return map.entrySet().stream()
      .map(es -> es.getValue())
      .reduce(0, (a, b) -> a + b);
  }

  public void printStats() {
    final Map<Integer, Integer> ddtHistogram = new HashMap<Integer, Integer>();
    for (int gamma = 1; gamma < sbox.size(); gamma++)
      for (int delta = 1; delta < sbox.size(); delta++)
        if (ddtMemo[gamma][delta] != 0)
          increaseEntryByOne(ddtHistogram, ddtMemo[gamma][delta]);
    System.out.println(ddtHistogram + " " + sumValues(ddtHistogram));
    final Map<Integer, Integer> bctHistogram = new HashMap<Integer, Integer>();
    for (int gamma = 1; gamma < sbox.size(); gamma++)
      for (int delta = 1; delta < sbox.size(); delta++)
        if (bctMemo[gamma][delta] != 0)
          increaseEntryByOne(bctHistogram, bctMemo[gamma][delta]);
    System.out.println(bctHistogram + " " + sumValues(bctHistogram));
    final Map<Integer, Integer> ubctHistogram = new HashMap<Integer, Integer>();
    ubctMemo.entrySet().stream()
      .filter(entry -> entry.getKey().getValue0() != 0 && entry.getKey().getValue1() != 0 && entry.getKey().getValue2() != 0)
      .forEach(entry -> increaseEntryByOne(ubctHistogram, entry.getValue()));
    System.out.println(ubctHistogram + " " + sumValues(ubctHistogram));
    final Map<Integer, Integer> lbctHistogram = new HashMap<Integer, Integer>();
    lbctMemo.entrySet().stream()
      .filter(entry -> entry.getKey().getValue0() != 0 && entry.getKey().getValue1() != 0 && entry.getKey().getValue2() != 0)
      .forEach(entry -> increaseEntryByOne(lbctHistogram, entry.getValue()));
    System.out.println(lbctHistogram + " " + sumValues(lbctHistogram));
    final Map<Integer, Integer> fbctHistogram = new HashMap<Integer, Integer>();
    fbctMemo.entrySet().stream()
      .filter(entry -> entry.getKey().getValue0() != 0 && entry.getKey().getValue1() != 0 && entry.getKey().getValue2() != 0 && entry.getKey().getValue3() != 0)
      .forEach(entry -> increaseEntryByOne(fbctHistogram, entry.getValue()));
    System.out.println(fbctHistogram + " " + sumValues(fbctHistogram));
  }
}
