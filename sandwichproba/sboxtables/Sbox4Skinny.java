package sandwichproba.sboxtables;

public class Sbox4Skinny extends Sbox {
  public Sbox4Skinny() {
    super(new int[]{12,6,9,0,1,10,2,11,3,8,5,13,4,14,7,15},
          new int[]{3,4,6,8,12,10,1,14,9,2,5,7,0,11,13,15});
  }

  public int size() {
    return 16;
  }
}
