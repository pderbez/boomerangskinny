package sandwichproba.sboxtables;

public abstract class Sbox {
  protected final int[] sbox;
  protected final int[] invSbox;

  protected Sbox(final int[] sbox, final int[] invSbox) {
    this.sbox = sbox;
    this.invSbox = invSbox;
  }

  public int get(final int x) {
    return sbox[x];
  }

  public int getInv(final int y) {
    return invSbox[y];
  }

  public abstract int size();
}
