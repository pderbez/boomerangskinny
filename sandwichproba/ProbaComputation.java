package sandwichproba;

import sandwichproba.sboxtransition.Formula;
import sandwichproba.trails.FormulaGeneration;
import sandwichproba.sboxtables.Sbox4Skinny;
import sandwichproba.sboxtables.Sbox8Skinny;
import sandwichproba.sboxtables.SboxAES;
import sandwichproba.sboxtables.SboxTables;
import sandwichproba.util.TreeVariable;
import sandwichproba.util.Variable;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ProbaComputation {
  private final int nbRounds;
  private final int[][] inputDifference;
  private final int[][][] upperKeyDifference;
  private final int[][] outputDifference;
  private final int[][][] lowerKeyDifference;

  public ProbaComputation(final int nbRounds,
                          final int[][] inputDifference, final int[][][] upperKeyDifference,
                          final int[][] outputDifference, final int[][][] lowerKeyDifference) {
    this.nbRounds = nbRounds;
    this.inputDifference = inputDifference;
    this.upperKeyDifference = upperKeyDifference;
    this.outputDifference = outputDifference;
    this.lowerKeyDifference = lowerKeyDifference;
  }

  public Double getProba() {
    final int tk = 3;
    final int r = 23;
    final int bits = 64;
    System.out.println("tk"+tk+" "+r+"r "+bits+"bits");
    //System.out.println("Generation of formula");
    final Formula formula = new FormulaGeneration(nbRounds,
                                                  inputDifference,
                                                  upperKeyDifference,
                                                  outputDifference,
                                                  lowerKeyDifference).getFormula();
    System.out.println(formula);
    List<Variable> vars = formula.getVariables().stream().sorted().collect(Collectors.toList());
    System.out.println(vars);
    SboxTables sboxTables = new SboxTables((bits == 64) ? new Sbox4Skinny() : new Sbox8Skinny());
    //SboxTables sboxTables = new SboxTables(new SboxAES());
    sboxTables.printStats();
    double result = 0.0;
    if (tk == 2 && r == 17 && bits == 64 ||
        tk == 1 && r == 14 && bits == 64) { // TK2 17r 64bits and tk1 14r 64bits
      TreeVariable tree = new TreeVariable(toSet(vars.get(0), vars.get(1), vars.get(11), vars.get(12), vars.get(13)),
                                           Arrays.asList(new TreeVariable(toSet(vars.get(9)),//7 | 3 4
                                                                          Arrays.asList(new TreeVariable(toSet(vars.get(10),vars.get(6))),
                                                                                        new TreeVariable(toSet(vars.get(4)),//10
                                                                                                         Arrays.asList(new TreeVariable(toSet(vars.get(3))),//6
                                                                                                                       new TreeVariable(toSet(vars.get(8))))))),//9
                                                         new TreeVariable(toSet(vars.get(7)),
                                                                          Arrays.asList(new TreeVariable(toSet(vars.get(2))),
                                                                                        new TreeVariable(toSet(vars.get(5))))),
                                                         new TreeVariable(toSet(vars.get(14),vars.get(15)))));
      tree.preetyPrint();
      result = formula.evaluateTree(sboxTables, tree, 0);
    }
    else if (tk == 0 && r == 11 && bits == 64 ||
             tk == 0 && r == 13 && bits == 128 ||
             tk == 1 && r == 15 && bits == 64 ||
             tk == 1 && r == 14 && bits == 128 ||
             tk == 1 && r == 15 && bits == 128 ||
             tk == 1 && r == 16 && bits == 128 ||
             tk == 1 && r == 17 && bits == 128 ||
             tk == 2 && r == 18 && bits == 64 ||
             tk == 2 && r == 18 && bits == 128 ||
             tk == 2 && r == 19 && bits == 64 ||
             tk == 2 && r == 19 && bits == 128 ||
             tk == 3 && r == 22 && bits == 64 ||
             tk == 3 && r == 23 && bits == 64 ||
             tk == 3 && r == 22 && bits == 128 ||
             tk == 3 && r == 23 && bits == 128 ||
             tk == 3 && r == 24 && bits == 128) {
      result = formula.evaluate(sboxTables);
    }
    /*else if (tk == 2 && r == 18 && bits == 128) { // TK2 18r 128bits
      TreeVariable tree = new TreeVariable(toSet(vars.get(0),vars.get(5)),
                                           Arrays.asList(new TreeVariable(toSet(vars.get(4)),
                                                                          Arrays.asList(new TreeVariable(toSet(vars.get(1),vars.get(3))),
                                                                                       new TreeVariable(toSet(vars.get(2))))),
                                                         new TreeVariable(toSet(vars.get(6), vars.get(7), vars.get(8)))));
      tree.preetyPrint();
      result = formula.evaluateTree(sboxTables, tree, 0);
      }*/
    return result;
  }

  private Set<Variable> toSet(Variable ... vars) {
    return Arrays.asList(vars).stream().collect(Collectors.toSet());
  }
}
