package sandwichproba.util;

/**
 * A class representing a variable. It contains an id, and the position in the trails
 *
 * @author Mathieu Vavrille
 */
public class Variable implements Comparable<Variable> {
  private static int created_counter = 0;

  public final int varNb;
  private final int posRound;
  private final int posI;
  private final int posJ;

  public Variable(final int posRound, final int posI, final int posJ) {
    this.posRound = posRound;
    this.posI = posI;
    this.posJ = posJ;
    varNb = created_counter;
    created_counter++;
  }

  public int getVarNb() {
    return varNb;
  }
  public int getPosRound() {
    return posRound;
  }
  public int getPosI() {
    return posI;
  }
  public int getPosJ() {
    return posJ;
  }

  public String toString() {
    return "V"+varNb+"-"+posRound+"-"+posI+"-"+posJ;
  }
  
  @Override
  public int compareTo(Variable other) {
    return Integer.compare(this.varNb, other.varNb);
}
  
}
