package sandwichproba.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A class representing expressiont of the form 'cst + v1 + v2 + ... + vn' where cst is an integer and vi's are variables
 *
 * @author Mathieu Vavrille
 */ 
public class XorExpr {
  private final int constant;
  private final List<Variable> variables;

  public XorExpr() {
    this(0, Collections.<Variable>emptyList());
  }

  public XorExpr(final int constant, final List<Variable> variables) {
    this.constant = constant;
    this.variables = new ArrayList<Variable>(variables);
  }

  public static XorExpr ofConstant(final int constant) {
    return new XorExpr(constant, Collections.<Variable>emptyList());
  }

  public static XorExpr ofVariable(final Variable var) {
    return new XorExpr(0, Collections.singletonList(var));
  }

  public XorExpr add(final int constant) {
    return new XorExpr(this.constant ^ constant, this.variables);
  }

  public XorExpr add(final Variable var) {
    List<Variable> newVariables = new ArrayList<Variable>(this.variables);
    newVariables.add(var);
    return new XorExpr(this.constant, newVariables);
  }

  /**
   * @return a list of variables appearing in the formula
   */
  public List<Variable> getVariables() {
    return variables;
  }

  public int getConstant() {
    return constant;
  }

  /**
   * @return true iff the expression is constant, ie has no variables involved
   */
  public boolean isConstant() {
    return variables.size() == 0;
  }

  public int toConstant() {
    if (isConstant())
      return constant;
    else
      throw new IllegalStateException(toString() + " is not a constant");
  }

  /**
   * @return true iff the expression is zero, ie the constant is zero and there is no variables involved
   */
  public boolean isZero() {
    return this.isConstant() && constant == 0;
  }

  public String toString() {
    if (variables.size() == 0)
      return String.valueOf(constant);
    String constantExpression = "";
    if (constant != 0)
      constantExpression = "" + constant + "+";
    return constantExpression+variables.stream().map(Variable::toString).collect(Collectors.joining("+"));
  }

  /**
   * @param affectation a mapping from variables to integers
   * @return a new expression where the variables appearing in the initial expression and has a entry in the map have been replaced by their associated value
   */
  public XorExpr evaluate(final Map<Variable,Integer> affectation) {
    return new XorExpr(variables.stream()
                       .filter(affectation::containsKey)
                       .reduce(constant, (acc, var) -> acc^affectation.get(var), (s1, s2) -> { throw new IllegalStateException("I am not using parallel computations"); }),
                       variables.stream()
                       .filter(var -> !affectation.containsKey(var))
                       .collect(Collectors.toList()));
  }

  /**
   * @param variable the variable to fix
   * @param value the value to which fix the variable
   * @return a new expression where the variable has been replaced by its value
   */
  public XorExpr imposeVariable(final Variable variable, final int value) {
    return new XorExpr(variables.stream()
                       .filter(currentVar -> currentVar == variable)
                       .reduce(constant, (acc, var) -> acc^value, (s1, s2) -> { throw new IllegalStateException("I am not using parallel computations"); }),
                       variables.stream()
                       .filter(currentVar -> currentVar != variable)
                       .collect(Collectors.toList()));
  }
}
