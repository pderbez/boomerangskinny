all: jar

jar: boomerangsearch
	jar cfm boomerangsearch.jar META-INF/boomerangManifest.txt boomerangsearch/

run1: boomerangsearch.jar
	java -ea -jar boomerangsearch.jar -verbose -nbRounds=12 -tk=1 -step1output=test.json -noStep2 -t=3
run2: boomerangsearch.jar
	java -jar boomerangsearch.jar -verbose -nbRounds=20 -tk=3 -blockSize=8 -step2input=brolyoutputs/tk3/step1-tk3-24-opt.json -step2output=output/step2-tk3-23-better.json -clusterGap=2 -nbStep2Sols=2 -t=2


tikz: solutiontotikz
	jar cfm solutiontotikz.jar META-INF/solutionToTikzManifest.txt boomerangsearch/

runtikz1: solutiontotikz.jar
	java -jar solutiontotikz.jar -step1input=patrickoutputs/step1-tk3-25-100-cheat.json -sol=0

runtikz2: solutiontotikz.jar
	java -jar solutiontotikz.jar -step2input=patrickoutputs/step2-tk3-25-128-cheat.json -step2best


step1:
	javac -cp /opt/gurobi902/linux64/lib/gurobi.jar:libs/* -d . step1/*.java

step2: step1
	javac -cp libs/*:. -d . step2/*.java

solutiontotikz: step1 step2
	javac -cp libs/*:. -d . solutiontotikz/*.java

boomerangsearch: step1 step2 solutiontotikz
	javac -cp /opt/gurobi902/linux64/lib/gurobi.jar:libs/*:. -d . *.java

clean:
	rm -rf boomerangsearch
	rm -f boomerangsearch.jar
	rm -f solutiontotikz.jar
	rm -f model.mps

.PHONY: step1 step2 boomerangsearch
