package boomerangsearch.step1;

import gurobi.*;

public class Step1Factory {
  private GRBModel model;
  private int regime;

  /**
   * @param model the main gurobi model
   * @param regime whether we are on SK (TK0), TK1, TK2 or TK3
   */
  public Step1Factory(final GRBModel model, final int regime) {
    this.model = model;
    this.regime = regime;
  }

  /** Adds the constraints of the linear part of the trails : ART, SR and MC */
  public void addLinear(GRBVar[][][] DX, GRBVar[][][] DTK) throws GRBException {
    for (int round = 0; round < DX.length-1; round++)
      for (int j = 0; j < 4; j++) {
        addXor(DX[round+1][1][j], DX[round][0][j], DTK[round][0][j]);
        addXor(DX[round+1][2][j], DX[round][1][(j+3)%4], DTK[round][1][(j+3)%4], DX[round][2][(j+2)%4]);
        addXor(DX[round+1][3][j], DX[round+1][1][j], DX[round][2][(j+2)%4]);
        addXor(DX[round+1][0][j], DX[round+1][3][j], DX[round][3][(j+1)%4]);
      }
  }

  /** Adds the constraints of the linear part of the trails for SK : ART, SR and MC */
  public void addLinearSK(GRBVar[][][] DX) throws GRBException {
    for (int round = 0; round < DX.length-1; round++)
      for (int j = 0; j < 4; j++) {
        model.addConstr(DX[round+1][1][j], GRB.EQUAL, DX[round][0][j], "");
        addXor(DX[round+1][2][j], DX[round][1][(j+3)%4], DX[round][2][(j+2)%4]);
        addXor(DX[round+1][3][j], DX[round+1][1][j], DX[round][2][(j+2)%4]);
        addXor(DX[round+1][0][j], DX[round+1][3][j], DX[round][3][(j+1)%4]);
      }
  }

  /** Adds constraints on the free variables between upper and lower trail */
  public void freePropagation(GRBVar[][][] DXup, GRBVar[][][] freeSBup, GRBVar[][][] DXlo, GRBVar[][][] freeXlo) throws GRBException {
    double[] t1 = {1.0, -1.0, -1.0, 1.0};
    double[] t2 = {2.0, -2.0, -1.0, 1.0, -1.0, 1.0};
    double[] t3 = {3.0, -3.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0};
    for (int r = 0; r < freeXlo.length-1; r++) {
      for (int j = 0; j < 4; j++) {
        model.addConstr(linExprOf(t3, freeSBup[r][0][j], DXup[r][0][j], freeSBup[r+1][0][j], freeXlo[r+1][0][j], freeSBup[r+1][1][j], freeXlo[r+1][1][j], freeSBup[r+1][3][j], freeXlo[r+1][3][j]), GRB.GREATER_EQUAL, -5.0, "");
        model.addConstr(linExprOf(t1, freeSBup[r][1][j], DXup[r][1][j], freeSBup[r+1][2][(j+1)%4], freeXlo[r+1][2][(j+1)%4]), GRB.GREATER_EQUAL, -1.0, "");
        model.addConstr(linExprOf(t3, freeSBup[r][2][j], DXup[r][2][j], freeSBup[r+1][2][(j+2)%4], freeXlo[r+1][2][(j+2)%4], freeSBup[r+1][2][(j+2)%4], freeXlo[r+1][2][(j+2)%4], freeSBup[r+1][3][(j+2)%4], freeXlo[r+1][3][(j+2)%4]), GRB.GREATER_EQUAL, -5.0, "");
        model.addConstr(linExprOf(t1, freeSBup[r][3][j], DXup[r][3][j], freeSBup[r+1][0][(j+3)%4], freeXlo[r+1][0][(j+3)%4]), GRB.GREATER_EQUAL, -1.0, "");

        model.addConstr(linExprOf(t1, freeXlo[r+1][0][j], DXlo[r+1][0][j], freeXlo[r][3][(j+1)%4], freeSBup[r][3][(j+1)%4]), GRB.GREATER_EQUAL, -1.0, "");
        model.addConstr(linExprOf(t3, freeXlo[r+1][1][j], DXlo[r+1][1][j], freeXlo[r][0][j], freeSBup[r][0][j], freeXlo[r][1][(j+3)%4], freeSBup[r][1][(j+3)%4], freeXlo[r][2][(j+2)%4], freeSBup[r][2][(j+2)%4]), GRB.GREATER_EQUAL, -5.0, "");
        model.addConstr(linExprOf(t1, freeXlo[r+1][2][j], DXlo[r+1][2][j], freeXlo[r][1][(j+3)%4], freeSBup[r][1][(j+3)%4]), GRB.GREATER_EQUAL, -1.0, "");
        model.addConstr(linExprOf(t3, freeXlo[r+1][3][j], DXlo[r+1][3][j], freeXlo[r][1][(j+3)%4], freeSBup[r][1][(j+3)%4], freeXlo[r][2][(j+2)%4], freeSBup[r][2][(j+2)%4], freeXlo[r][3][(j+1)%4], freeSBup[r][3][(j+1)%4]), GRB.GREATER_EQUAL, -5.0, "");
      }
    }
  }

  /** Adds constraints on the free variables in the upper trail */
  public void freePropagationUpper(GRBVar[][][] freeX, GRBVar[][][] freeSB, GRBVar[][][] DX) throws GRBException {
    for (int r = 0; r < freeX.length-1; r++)
      for (int j = 0; j < 4; j++) {
        addOr(freeX[r+1][0][j], freeSB[r][0][j], freeSB[r][2][(j+2)%4], freeSB[r][3][(j+1)%4]);
        model.addConstr(freeX[r+1][1][j], GRB.EQUAL, freeSB[r][0][j], "");
        addOr(freeX[r+1][2][j], freeSB[r][1][(j+3)%4], freeSB[r][2][(j+2)%4]);
        addOr(freeX[r+1][3][j], freeSB[r][0][j], freeSB[r][2][(j+2)%4]);
      }
  }

  /** Adds constraints on the free variables in the upper trail */
  public void freePropagationLower(GRBVar[][][] freeX, GRBVar[][][] freeSB, GRBVar[][][] DX) throws GRBException {
    for (int r = 0; r < freeX.length-1; r++)
      for (int j = 0; j < 4; j++) {
        model.addConstr(freeSB[r][0][j], GRB.EQUAL, freeX[r+1][1][j], "");
        addOr(freeSB[r][1][j], freeX[r+1][1][(j+1)%4],freeX[r+1][2][(j+1)%4],freeX[r+1][3][(j+1)%4]);
        addOr(freeSB[r][2][j], freeX[r+1][1][(j+2)%4], freeX[r+1][3][(j+2)%4]);
        addOr(freeSB[r][3][j], freeX[r+1][0][(j+3)%4], freeX[r+1][3][(j+3)%4]);
      }
  }

  public void objectiveConstraints(GRBVar[][][] DXupper, GRBVar[][][] freeXupper, GRBVar[][][] freeSBupper, GRBVar[][][] DXlower, GRBVar[][][] freeXlower, GRBVar[][][] freeSBlower, GRBVar[][][] isTable, GRBVar[][][] isDDT2) throws GRBException {
    for (int round = 0; round < DXupper.length; round++)
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++) {
          oneObjectiveConstraint(DXupper[round][i][j], freeXupper[round][i][j], freeSBupper[round][i][j], DXlower[round][i][j], freeXlower[round][i][j], freeSBlower[round][i][j], isTable[round][i][j], isDDT2[round][i][j]);
        }
  }

  public void oneObjectiveConstraint(GRBVar DXupper, GRBVar freeXupper, GRBVar freeSBupper, GRBVar DXlower, GRBVar freeXlower, GRBVar freeSBlower, GRBVar isTable, GRBVar isDDT2) throws GRBException {
    model.addConstr(DXupper, GRB.GREATER_EQUAL, freeSBupper, ""); // if free then =1 -> cy' exy
    model.addConstr(DXlower, GRB.GREATER_EQUAL, freeXlower,  ""); // Symmetry        -> cz' ezt
    model.addConstr(freeSBupper, GRB.GREATER_EQUAL, freeXupper, ""); // free propagates -> cy cx'
    model.addConstr(freeXlower, GRB.GREATER_EQUAL, freeSBlower, ""); // Symmetry        -> cz ct'
    model.addConstr(isTable, GRB.GREATER_EQUAL, isDDT2, ""); // DDT2 -> isTable -> d1 d2'
    model.addConstr(2.0, GRB.GREATER_EQUAL, linExprOf(freeSBupper, freeXlower, isTable), ""); // BCT and others -> cy' cz' d1'
    model.addConstr(linExprOf(freeSBupper, isTable), GRB.GREATER_EQUAL, DXupper, ""); //          -> cy exy' d1
    model.addConstr(linExprOf(freeXlower,  isTable), GRB.GREATER_EQUAL, DXlower, ""); // Symmetry -> cz ezt' d1
    model.addConstr(DXupper, GRB.GREATER_EQUAL, linExprOf(-1.0, freeXlower,  isTable), ""); //          -> cz' exy d1'
    model.addConstr(DXlower, GRB.GREATER_EQUAL, linExprOf(-1.0, freeSBupper, isTable), ""); // Symmetry -> cy' ezt d1'
    model.addConstr(linExprOf(DXupper, DXlower), GRB.GREATER_EQUAL, isTable, ""); // -> exy ezt d1'
    model.addConstr(isDDT2, GRB.GREATER_EQUAL, linExprOf(-1.0, freeXupper,  DXlower), ""); //          -> cx' ezt' d2
    model.addConstr(isDDT2, GRB.GREATER_EQUAL, linExprOf(-1.0, freeSBlower, DXupper), ""); // Symmetry -> ct' exy' d2
    model.addConstr(linExprOf(freeXupper, freeSBlower), GRB.GREATER_EQUAL, isDDT2, ""); // cx ct d2'
  }

  public void objectiveConstraintsDouble(GRBVar[][][] DXupper, GRBVar[][][] freeXupper, GRBVar[][][] freeSBupper, GRBVar[][][] DXlower, GRBVar[][][] freeXlower, GRBVar[][][] freeSBlower, GRBVar[][][] isTable, GRBVar[][][] isDDT2) throws GRBException {
    for (int round = 0; round < DXupper.length; round++)
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++) {
          oneObjectiveConstraintDouble(DXupper[round][i][j], freeXupper[round][i][j], freeSBupper[round][i][j], DXlower[round][i][j], freeXlower[round][i][j], freeSBlower[round][i][j], isTable[round][i][j], isDDT2[round][i][j]);
        }
  }

  public void oneObjectiveConstraintDouble(GRBVar DXupper, GRBVar freeXupper, GRBVar freeSBupper, GRBVar DXlower, GRBVar freeXlower, GRBVar freeSBlower, GRBVar isTable, GRBVar isDDT2) throws GRBException {
    boolean optimized = false;
    model.addConstr(DXupper, GRB.GREATER_EQUAL, freeSBupper, ""); // if free then =1  -> cy' exy
    model.addConstr(DXlower, GRB.GREATER_EQUAL, freeXlower,  ""); // Symmetry         -> cz' ezt
    if (!optimized) {
      model.addConstr(DXupper, GRB.GREATER_EQUAL, isDDT2, ""); // need value for ddt2 -> exy d2'
      model.addConstr(DXlower, GRB.GREATER_EQUAL, isDDT2, ""); // Symmetry            -> ezt d2'
    }
    else {
      model.addConstr(linExprOf(new double[]{1.0,1.0,-2.0}, DXupper, DXlower, isDDT2), GRB.GREATER_EQUAL, 0, "");
    }
    model.addConstr(freeSBupper, GRB.GREATER_EQUAL, freeXupper, ""); // free propagates  -> cy cx'
    model.addConstr(freeXlower, GRB.GREATER_EQUAL, freeSBlower, ""); // Symmetry         -> cz ct'
    model.addConstr(1.0, GRB.GREATER_EQUAL, linExprOf(isTable, isDDT2), ""); // not twice at the same time -> d1' d2'
    if (!optimized) {
      model.addConstr(2.0, GRB.GREATER_EQUAL, linExprOf(freeSBupper, freeXlower, isTable), ""); // BCT and others -> cy' cz' d1'
      model.addConstr(2.0, GRB.GREATER_EQUAL, linExprOf(freeSBupper, freeXlower, isDDT2),  ""); // Same for DDT2  -> cy' cz' d2'
    }
    else {
      model.addConstr(4, GRB.GREATER_EQUAL, linExprOf(new double[]{2.0,2.0,1.0,1.0}, freeSBupper, freeXlower, isTable, isDDT2), "");
    }
    model.addConstr(linExprOf(freeSBupper, freeSBlower, isTable), GRB.GREATER_EQUAL, DXupper, ""); //          -> cy ct exy' d1
    model.addConstr(linExprOf(freeXlower,  freeXupper,  isTable), GRB.GREATER_EQUAL, DXlower, ""); // Symmetry -> cx cz ezt' d1
    model.addConstr(DXupper, GRB.GREATER_EQUAL, linExprOf(-1.0, freeXlower,  isTable), ""); //          -> cz' exy d1'
    model.addConstr(DXlower, GRB.GREATER_EQUAL, linExprOf(-1.0, freeSBupper, isTable), ""); // Symmetry -> cy' ezt d1'
    model.addConstr(linExprOf(DXupper, DXlower), GRB.GREATER_EQUAL, isTable, ""); // need one non zero for table -> exy ezt d1'
    model.addConstr(isDDT2, GRB.GREATER_EQUAL, linExprOf(-1.0, freeXupper,  DXlower), ""); //          -> cx' ezt' d2
    model.addConstr(isDDT2, GRB.GREATER_EQUAL, linExprOf(-1.0, freeSBlower, DXupper), ""); // Symmetry -> ct' exy' d2
  }

  public void addXor(GRBVar ... vars) throws GRBException {
    for (int i = 0; i < vars.length; i++) {
      GRBLinExpr sumOthers = new GRBLinExpr();
      for (int j = 0; j < vars.length; j++)
        if (j != i)
          sumOthers.addTerm(1.0, vars[j]);
      model.addConstr(vars[i], GRB.LESS_EQUAL, sumOthers, "Xor");
    }
  }

  public void addOr(GRBVar mainVar, GRBVar ... vars) throws GRBException {
    if (true)
      model.addGenConstrOr(mainVar, vars, "");
    else {
      GRBLinExpr sumOthers = new GRBLinExpr();
      for (int i = 0; i < vars.length; i++) {
          sumOthers.addTerm(1.0, vars[i]);
          model.addConstr(vars[i], GRB.LESS_EQUAL, mainVar, "");
      }
      model.addConstr(mainVar, GRB.LESS_EQUAL, sumOthers, "Or");
    }
  }

  /** Ensures that there is no X round of DX and DTK to zero, where X depends on the regime */
  public void ensureNonZeroDifference(GRBVar[][][] DX, GRBVar[][][] DTK) throws GRBException {
    int nbNonZeroRounds = 0;
    switch (regime) {
    case 3: nbNonZeroRounds = 6;
      break;
    case 2: nbNonZeroRounds = 4;
      break;
    case 1: nbNonZeroRounds = 2;
      break;
    case 0: nbNonZeroRounds = 0;
      break;
    }
    if (DX.length >= nbNonZeroRounds+1) {
      GRBLinExpr nonZeroSum = new GRBLinExpr();
      for (int round = 0; round < nbNonZeroRounds; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            nonZeroSum.addTerm(1.0, DX[round][i][j]);
            if (i < 2)
              nonZeroSum.addTerm(1.0, DTK[round][i][j]);
          }
      model.addConstr(nonZeroSum, GRB.GREATER_EQUAL, 1, "nonZeroDiff");
    }
  }

  /** Remove symmetries in SK */
  public void removeSymmetriesSK(GRBVar[][] DX0) throws GRBException {
    // First row
    GRBLinExpr previousLines = new GRBLinExpr();
    for (int i = 0; i < 4; i++) {
      GRBLinExpr firstZero = new GRBLinExpr();
      firstZero.addTerm(3.0, DX0[i][0]);
      firstZero.multAdd(3.0, previousLines);
      model.addConstr(firstZero, GRB.GREATER_EQUAL, linExprOf(DX0[i][1],DX0[i][2],DX0[i][3]), "");
      model.addConstr(previousLines, GRB.GREATER_EQUAL, linExprOf(new double[]{-1.0,-1.0,1.0}, DX0[i][1], DX0[i][2], DX0[i][3]), "");
      previousLines.add(linExprOf(DX0[i]));
    }
  }

  public GRBLinExpr linExprOf(double[] coeffs, GRBVar ... vars) throws GRBException {
    GRBLinExpr ofVars = new GRBLinExpr();
    ofVars.addTerms(coeffs, vars);
    return ofVars;
  }

  public GRBLinExpr linExprOf(double constant, GRBVar ... vars) {
    GRBLinExpr ofVars = linExprOf(vars);
    ofVars.addConstant(constant);
    return ofVars;
  }

  public GRBLinExpr linExprOf(GRBVar ... vars) {
    GRBLinExpr expr = new GRBLinExpr();
    for (GRBVar var : vars)
      expr.addTerm(1.0, var);
    return expr;
  }

  public GRBLinExpr sumState(GRBVar[][] state) throws GRBException {
    GRBLinExpr sum = new GRBLinExpr();
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
        sum.addTerm(1.0, state[i][j]);
    return sum;
  }

  public void addKnownDiffBounds(GRBVar[][][] DX) throws GRBException {
    int[] diffBounds = new int[0];
    switch (regime) {
    case 0:
      diffBounds = new int[]{0,1,2,5,8,12,16,26,36,41,46,51,55,58,61,66,75,82,88,92,96,102,108,114,116,124,132,138,136,148,158};
      break;
    case 1:
      diffBounds = new int[]{0,0,0,1,2,3,6,10,13,16,23,32,38,41,45,49,54,59,62,66,70,75,79,83,85,88,95,102,108,112,120};
      break;
    case 2:
      diffBounds = new int[]{0,0,0,0,0,1,2,3,6,9,12,16,21,25,31,35,40,43,46,52,57,59,64,67,72,75,82,85,88,92,96};
      break;
    case 3:
      diffBounds = new int[]{0,0,0,0,0,0,0,1,2,3,6,10,13,16,19,24,27,31,35,43,45,48,51,55,58,60,65,72,77,81,85};
      break;
    }
    for (int start = 0; start < DX.length; start++)
      for (int end = start + 4; end < DX.length; end++) {
        GRBLinExpr sumRounds = new GRBLinExpr();
        for (int round = start; round <= end; round++)
          sumRounds.add(sumState(DX[round]));
        model.addConstr(sumRounds, GRB.GREATER_EQUAL, diffBounds[end-start+1], "");
      }
  }

  public void addKnownBoomBounds(GRBVar[][][] isTable, GRBVar[][][] isDDT2) throws GRBException {
    int[] diffBounds = new int[0];
    int minDiffIndex = 0;
    switch (regime) {
    case 0:
      diffBounds = new int[]{0,0,0,0,0,2,6,11,16,23,30,40,51,58,61,66,75,82,88,92,96};
      minDiffIndex = 7;
      break;
    case 1:
      diffBounds = new int[]{0,0,0,0,0,0,0,0 ,2 ,4 ,6 ,10,13,19,27,33,41,47,54,61,70};
      minDiffIndex = 11;
      break;
    case 2:
      diffBounds = new int[]{0,0,0,0,0,0,0,0 ,0 ,0 ,0 ,0 ,2 ,4 ,6 ,10,13,19,25,31,36,43,51};
      minDiffIndex = 15;
      break;
    case 3:
      diffBounds = new int[]{0,0,0,0,0,0,0,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,2 ,4 ,6 ,10,13,19,26,33,40};
      minDiffIndex = 19;
      break;
    }
    for (int boomlength = minDiffIndex; boomlength < isTable.length-1; boomlength++)
      for (int start = 0; start <= isTable.length-boomlength; start++) {
        GRBLinExpr sumRounds = new GRBLinExpr();
        for (int round = start; round < start+boomlength; round++) {
          sumRounds.add(sumState(isTable[round]));
          sumRounds.add(sumState(isDDT2[round]));
        }
        model.addConstr(sumRounds, GRB.GREATER_EQUAL, diffBounds[boomlength], "");
      }
  }
}
