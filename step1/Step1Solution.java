package boomerangsearch.step1;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.io.File;
import java.io.IOException;

public class Step1Solution {
  public int nbRounds;
  public int regime;
  public int objective;
  public int[][][] DXupper;
  public int[][][] freeXupper;
  public int[][][] freeSBupper;
  public Step1SolutionTweakey DTKupper;
  public int[][][] DXlower;
  public int[][][] freeXlower;
  public int[][][] freeSBlower;
  public Step1SolutionTweakey DTKlower;
  public int[][][] isTable;
  public int[][][] isDDT2;

  public Step1Solution() {}

  public Step1Solution(int nbRounds, int regime, int objective, int[][][] DXupper, int[][][] freeXupper, int[][][] freeSBupper, Step1SolutionTweakey DTKupper, int[][][] DXlower, int[][][] freeXlower, int[][][] freeSBlower, Step1SolutionTweakey DTKlower, int[][][] isTable, int[][][] isDDT2) {
    this.nbRounds = nbRounds;
    this.regime = regime;
    this.objective = objective;
    this.DXupper = DXupper;
    this.freeXupper = freeXupper;
    this.freeSBupper = freeSBupper;
    this.DTKupper = DTKupper;
    this.DXlower = DXlower;
    this.freeXlower = freeXlower;
    this.freeSBlower = freeSBlower;
    this.DTKlower = DTKlower;
    this.isTable = isTable;
    this.isDDT2 = isDDT2;
  }

  public void toFile(String fileName) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      mapper.writeValue(new File(fileName), this);
    }
    catch (JsonParseException e) { e.printStackTrace(); System.exit(1); }
    catch (JsonMappingException e) { e.printStackTrace(); System.exit(1); }
    catch (IOException e) { e.printStackTrace(); System.exit(1); }
  }

  public static void toFile(String fileName, List<Step1Solution> solutions) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      mapper.writeValue(new File(fileName), solutions);
    }
    catch (JsonParseException e) { e.printStackTrace(); System.exit(1); }
    catch (JsonMappingException e) { e.printStackTrace(); System.exit(1); }
    catch (IOException e) { e.printStackTrace(); System.exit(1); }
  }

  public static void toFile(File file, List<Step1Solution> solutions) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      mapper.writeValue(file, solutions);
    }
    catch (JsonParseException e) { e.printStackTrace(); System.exit(1); }
    catch (JsonMappingException e) { e.printStackTrace(); System.exit(1); }
    catch (IOException e) { e.printStackTrace(); System.exit(1); }
  }

  public static List<Step1Solution> fromFile(String fileName) {
    return fromFile(new File(fileName));
  }

  public static List<Step1Solution> fromFile(File file) {
    try {
      return new ObjectMapper().readValue(file, new TypeReference<List<Step1Solution>>(){});
    }
    catch (JsonParseException e) { e.printStackTrace(); System.exit(1); }
    catch (JsonMappingException e) { e.printStackTrace(); System.exit(1); }
    catch (IOException e) { e.printStackTrace(); System.exit(1); }
    return null; // Can't reach
  }
}
