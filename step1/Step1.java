package boomerangsearch.step1;

import gurobi.*;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.File;

public class Step1 {
  private final int nbRounds;
  private final int regime;
  private final GRBModel model;
  private final Step1Factory factory;
  private final GRBVar[][][] DXupper;
  private final GRBVar[][][] freeXupper;
  private final GRBVar[][][] freeSBupper;
  private final Step1Tweakey DTKupper;
  private final GRBVar[][][] DXlower;
  private final GRBVar[][][] freeXlower;
  private final GRBVar[][][] freeSBlower;
  private final Step1Tweakey DTKlower;
  private final GRBVar[][][] isDDT2;
  private final GRBVar[][][] isTable;
  private final GRBLinExpr   objective;

  /**
   * @param env the Gurobi environment
   * @param nbRounds the number of rounds of the boomerang
   * @param regime the regime of the analysis. 0, 1, 2 or 3 for SK, TK1, TK2 or TK3
   */
  public Step1(final GRBEnv env, final int nbRounds, final int regime) throws GRBException {
    model = new GRBModel(env);
    this.nbRounds = nbRounds;
    this.regime = regime;
    factory = new Step1Factory(model, regime);
    DXupper = new GRBVar[nbRounds][4][4];
    freeXupper = new GRBVar[nbRounds][4][4];
    freeSBupper = new GRBVar[nbRounds][4][4];
    DTKupper = (regime == 0) ? null : new Step1Tweakey(model, nbRounds, regime);
    DXlower = new GRBVar[nbRounds][4][4];
    freeXlower = new GRBVar[nbRounds][4][4];
    freeSBlower = new GRBVar[nbRounds][4][4];
    DTKlower = (regime == 0) ? null : new Step1Tweakey(model, nbRounds, regime);
    isDDT2 = new GRBVar[nbRounds][4][4];
    isTable = new GRBVar[nbRounds][4][4];
    // Initialization
    for (int round = 0; round < nbRounds; round++)
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++) {
          DXupper[round][i][j]     = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "DXupper"+round+i+j);
          freeXupper[round][i][j]  = model.addVar(0.0, 1.0, (round<=nbRounds/2)? 0.0 : 1.0, GRB.BINARY, "freeXupper"+round+i+j);
          freeSBupper[round][i][j] = model.addVar(0.0, 1.0, (round<=nbRounds/2)? 0.0 : 1.0, GRB.BINARY, "freeSBupper"+round+i+j);
          DXlower[round][i][j]     = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "DXlower"+round+i+j);
          freeXlower[round][i][j]  = model.addVar(0.0, 1.0, (round>=nbRounds/2)? 0.0 : 1.0, GRB.BINARY, "freeXlower"+round+i+j);
          freeSBlower[round][i][j] = model.addVar(0.0, 1.0, (round>=nbRounds/2)? 0.0 : 1.0, GRB.BINARY, "freeSBlower"+round+i+j);
          isDDT2[round][i][j] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "isDDT2"+round+i+j);
          isTable[round][i][j] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "isTable"+round+i+j);
        }

    //articleTrails(128);

    // Constraints
    if (regime == 0) {
      System.out.println("TODO, can replace the equality a=f in MC by a reference");
      factory.addLinearSK(DXupper);
      factory.addLinearSK(DXlower);
      factory.removeSymmetriesSK(DXupper[0]);
      model.addConstr(factory.sumState(DXupper[0]), GRB.GREATER_EQUAL, 1, "");
      model.addConstr(factory.sumState(DXlower[nbRounds-1]), GRB.GREATER_EQUAL, 1, "");
      /*for (int r = 0; r < nbRounds; r++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            GRBLinExpr s = new GRBLinExpr();
            s.addTerms(new double[]{1.0,1.0,1.0,1.0}, new GRBVar[]{freeXupper[r][i][j],freeSBupper[r][i][j],freeXlower[r][i][j],freeSBlower[r][i][j]});
            model.addConstr(s, GRB.GREATER_EQUAL, 1,"");
            }*/
    }
    else {
      factory.addLinear(DXupper, DTKupper.DTK);
      factory.addLinear(DXlower, DTKlower.DTK);
    }

    factory.freePropagationUpper(freeXupper, freeSBupper, DXupper);
    factory.freePropagationLower(freeXlower, freeSBlower, DXlower);
    //factory.freePropagation(DXupper, freeSBupper, DXlower, freeXlower);

    factory.objectiveConstraints(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower, isTable, isDDT2);

    factory.addKnownDiffBounds(DXupper);
    factory.addKnownDiffBounds(DXlower);

    //factory.addKnownBoomBounds(isTable, isDDT2);

    objective = new GRBLinExpr();
    for (int round = 0; round < nbRounds; round++)
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++) {
          objective.addTerm(1.0, isTable[round][i][j]);
          objective.addTerm(1.0, isDDT2[round][i][j]);
        }
    model.setObjective(objective, GRB.MINIMIZE);
  }

  public List<Step1Solution> solve(final int nbSolutions, final boolean nonOptimalSolutions, final int minObjValue, final int nbThreads) throws GRBException {
    model.read("tune1.prm");
    //model.write("model.mps");
    model.set(GRB.IntParam.Threads, nbThreads);
    if (minObjValue != -1)
      model.addConstr(objective, GRB.GREATER_EQUAL, minObjValue, "objectiveFix");
    GRBConstr c0 = model.addConstr(freeXlower[nbRounds/2][0][0], GRB.EQUAL, 1.0, "");
    GRBConstr c1 = model.addConstr(freeSBupper[nbRounds/2][0][0], GRB.EQUAL, 1.0, "");
    model.optimize();
    model.remove(c0);
    model.remove(c1);
    /*model.set(GRB.DoubleParam.PoolGap, (nonOptimalSolutions) ? 1.0 : 0.005);
    model.set(GRB.IntParam.PoolSolutions, nbSolutions);
    model.set(GRB.IntParam.PoolSearchMode, 2);*/
    model.optimize();
    return getAllFoundSolutions();
  }

  public void dispose() throws GRBException {
    model.dispose();
  }

  public List<Step1Solution> getAllFoundSolutions() throws GRBException {
    return IntStream.range(0, model.get(GRB.IntAttr.SolCount)).boxed()
      .map(solNb -> getSolution(solNb))
      .collect(Collectors.toList());
  }

  private Step1Solution getSolution(final int solutionNumber) {
    try {
      model.set(GRB.IntParam.SolutionNumber, solutionNumber);
      int[][][] DXupperValue     = new int[nbRounds][4][4];
      int[][][] freeXupperValue  = new int[nbRounds][4][4];
      int[][][] freeSBupperValue = new int[nbRounds][4][4];
      int[][][] DXlowerValue     = new int[nbRounds][4][4];
      int[][][] freeXlowerValue  = new int[nbRounds][4][4];
      int[][][] freeSBlowerValue = new int[nbRounds][4][4];
      int[][][] isTableValue     = new int[nbRounds][4][4];
      int[][][] isDDT2Value      = new int[nbRounds][4][4];
      for (int round = 0; round < nbRounds; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            DXupperValue[round][i][j] = (int) Math.round(DXupper[round][i][j].get(GRB.DoubleAttr.Xn));
            freeXupperValue[round][i][j] = (int) Math.round(freeXupper[round][i][j].get(GRB.DoubleAttr.Xn));
            freeSBupperValue[round][i][j] = (int) Math.round(freeSBupper[round][i][j].get(GRB.DoubleAttr.Xn));
            DXlowerValue[round][i][j] = (int) Math.round(DXlower[round][i][j].get(GRB.DoubleAttr.Xn));
            freeXlowerValue[round][i][j] = (int) Math.round(freeXlower[round][i][j].get(GRB.DoubleAttr.Xn));
            freeSBlowerValue[round][i][j] = (int) Math.round(freeSBlower[round][i][j].get(GRB.DoubleAttr.Xn));
            isTableValue[round][i][j] = (int) Math.round(isTable[round][i][j].get(GRB.DoubleAttr.Xn));
            isDDT2Value[round][i][j] = (int) Math.round(isDDT2[round][i][j].get(GRB.DoubleAttr.Xn));
          }
      return new Step1Solution(nbRounds, regime, (int) Math.round(model.get(GRB.DoubleAttr.PoolObjVal)), DXupperValue, freeXupperValue, freeSBupperValue, (regime == 0) ? null : DTKupper.getValue(), DXlowerValue, freeXlowerValue, freeSBlowerValue, (regime == 0) ? null : DTKlower.getValue(), isTableValue, isDDT2Value);
    } catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " + e.getMessage());
      e.printStackTrace();
      System.exit(1);
      return null; // Can't access
    }
  }

  private void articleTrails(final int bits) throws GRBException {
    // -------- Article trails --------
    // SKINNY-64-128
    if (bits == 64 && regime == 2 && nbRounds == 17) {
      for (int round = 0; round < 7; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 0 && i == 3 && j == 3 ||
                round == 1 && i == 0 && j == 2 ||
                round == 6 && i == 2 && j == 3)
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
      for (int round = 10; round < 17; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 10 && i == 0 && j == 1 ||
                round == 10 && i == 2 && j == 3 ||
                round == 11 && i == 1 && j == 1 ||
                round == 15 && i == 2 && j == 2 ||
                round == 16 && i == 0 && j == 0 ||
                round == 16 && i == 3 && j == 0)
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
    }
    // SKINNY-128-256
    else if (bits == 128 && regime == 2 && nbRounds == 18) {
      for (int round = 0; round < 6; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (/*round == 0 && i == 3 && j == 3 ||
                round == 1 && i == 1 && j == 2 ||
                round == 1 && i == 3 && j == 2 ||
                round == 2 && i == 0 && j == 1 ||
                round == 2 && i == 2 && j == 3 ||
                round == 3 && i == 1 && j == 1 ||
                round == 7 && i == 2 && j == 2*/
                round == 0 && i == 3 && j == 1 ||
                round == 1 && i == 0 && j == 0 )
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
      for (int round = 13; round < 18; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (/*round == 11 && i == 0 && j == 2 ||
                round == 16 && i == 2 && j == 3 ||
                round == 17 && i == 0 && j == 1 ||
                round == 17 && i == 2 && j == 1 ||
                round == 17 && i == 3 && j == 1*/
                round == 17 && i == 0 && j == 0 ||
                round == 17 && i == 3 && j == 0 ||
                round == 16 && i == 2 && j == 2)
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
      model.addConstr(freeXupper[12][1][1], GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXupper[11][0][1], GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXupper[11][2][3], GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXupper[10][1][2], GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXupper[10][3][2], GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXupper[9][3][3] , GRB.EQUAL, 0.0, "debug");
      model.addConstr(freeXlower[6][2][1], GRB.EQUAL, 1.0, "debug");
      model.addConstr(freeSBlower[6][2][1], GRB.EQUAL, 0.0, "debug");
      model.addConstr(DXlower[6][2][1], GRB.EQUAL, 1.0, "debug");
    }
    // SKINNY-64-192 and SKINNY-128-384
    else if ((bits == 64 || bits == 128) && regime == 3 && nbRounds == 22) {
      for (int round = 0; round < 10; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 0 && i == 0 && j == 1 ||
                round == 0 && i == 1 && j == 0 ||
                round == 0 && i == 2 && j == 3 ||
                round == 0 && i == 3 && j == 2 ||
                round == 1 && i == 3 && j == 1 ||
                round == 2 && i == 0 && j == 0 ||
                round == 9 && i == 2 && j == 3)
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
      for (int round = 12; round < 22; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 12 && i == 3 && j == 1 ||
                round == 13 && i == 0 && j == 0 ||
                round == 20 && i == 2 && j == 3 ||
                round == 21 && i == 0 && j == 1 ||
                round == 21 && i == 2 && j == 1 ||
                round == 21 && i == 3 && j == 1)
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
    }
    else if (nbRounds == 23 && regime == 3 && bits == 128) {
      for (int round = 1; round < 8; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 1 && i == 3 && j == 1 ||
                round == 2 && i == 0 && j == 0)
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXupper[round][i][j], GRB.EQUAL, 0.0, "debug");
          }
      for (int round = 16; round < 22; round++)
        for (int i = 0; i < 4; i++)
          for (int j = 0; j < 4; j++) {
            if (round == 21 && i == 2 && j == 3)
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 1.0, "debug");
            else
              model.addConstr(DXlower[round][i][j], GRB.EQUAL, 0.0, "debug");
              }
    }
  }
}
