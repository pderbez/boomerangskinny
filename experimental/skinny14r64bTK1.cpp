#include <iostream>
#include <random>
#include <vector>
#include <execution>
#include <mutex>

using namespace std;

uint8_t sbox[16] = {0xc, 6, 9, 0, 1, 0xa, 2, 0xb, 3, 8, 5, 0xd, 4, 0xe, 7, 0xf};
uint8_t p[16] = {0, 5,  10, 15, 4,  9, 14, 3, 8, 13, 2,  7,  12, 1, 6,  11};

uint64_t SubNibbles(uint64_t x)
{
    uint64_t bit0 = ~x;
    uint64_t bit1 = bit0 >> 1;
    uint64_t bit2 = bit0 >> 2;
    uint64_t bit3 = bit0 >> 3;
    bit0 ^= bit3 & bit2;
    bit3 ^= bit1 & bit2;
    bit2 ^= bit1 & bit0;
    bit1 ^= bit0 & bit3;
    x = ((bit0 << 3) & 0x8888888888888888ULL) |
        ( bit1       & 0x1111111111111111ULL) |
        ((bit2 << 1) & 0x2222222222222222ULL) |
        ((bit3 << 2) & 0x4444444444444444ULL);
    return ~x;
}

uint64_t invSubNibbles(uint64_t x)
{
    uint64_t bit0 = ~x;
    uint64_t bit1 = bit0 >> 1;
    uint64_t bit2 = bit0 >> 2;
    uint64_t bit3 = bit0 >> 3;
    bit0 ^= bit3 & bit2;
    bit1 ^= bit3 & bit0;
    bit2 ^= bit1 & bit0;
    bit3 ^= bit1 & bit2;
    x = ((bit0 << 1) & 0x2222222222222222ULL) |
        ((bit1 << 2) & 0x4444444444444444ULL) |
        ((bit2 << 3) & 0x8888888888888888ULL) |
        ( bit3       & 0x1111111111111111ULL);
    return ~x;
}

uint64_t ShiftCells(uint64_t x) {
  uint64_t y = x & 0x000F000F000F000FULL;
  y |= (x << 16) & 0x00F000F000F00000ULL;
  y |= (x >> 48) & 0x00000000000000F0ULL;
  y |= (x << 32) & 0x0F000F0000000000ULL;
  y |= (x >> 32) & 0x000000000F000F00ULL;
  y |= (x << 48) & 0xF000000000000000ULL;
  y |= (x >> 16) & 0x0000F000F000F000ULL;
  return y;
}

uint64_t invShiftCells(uint64_t x) {
  uint64_t y = x & 0x000F000F000F000FULL;
  y |= (x >> 16) & 0x000000F000F000F0ULL;
  y |= (x << 48) & 0x00F0000000000000ULL;
  y |= (x >> 32) & 0x000000000F000F00ULL;
  y |= (x << 32) & 0x0F000F0000000000ULL;
  y |= (x >> 48) & 0x000000000000F000ULL;
  y |= (x << 16) & 0xF000F000F0000000ULL;
  return y;
}

uint64_t MixColumns(uint64_t x) {
  uint64_t const & line0 = x & 0x000F000F000F000FULL;
  uint64_t const & line1 = (x & 0x00F000F000F000F0ULL) >> 4;
  uint64_t const & line2 = (x & 0x0F000F000F000F00ULL) >> 8;
  uint64_t const & line3 = (x & 0xF000F000F000F000ULL) >> 12;

  uint64_t const & u3 = (line0 ^ line2);
  uint64_t y = (u3 ^ line3);
  y |= line0 << 4;
  y |= (line1 ^ line2) << 8;
  y |= u3 << 12;
  return y;
}

uint64_t invMixColumns(uint64_t x) {
  uint64_t const & line0 = x & 0x000F000F000F000FULL;
  uint64_t const & line1 = (x & 0x00F000F000F000F0ULL) >> 4;
  uint64_t const & line2 = (x & 0x0F000F000F000F00ULL) >> 8;
  uint64_t const & line3 = (x & 0xF000F000F000F000ULL) >> 12;

  uint64_t y = line1;
  y |= (line1 ^ line2 ^ line3) << 4;
  y |= (line1 ^ line3) << 8;
  y |= (line0 ^ line3) << 12;

  return y;
}

uint64_t Round(uint64_t x, uint64_t k) { // one round of SKINNY
  x = SubNibbles(x);
  x ^= k;
  x ^= uint64_t(2) << 8; // round constant (other part is masked by random key)
  x = ShiftCells(x);
  return MixColumns(x);
}

uint64_t LastRound(uint64_t x, uint64_t k) { // one round of SKINNY
  x = SubNibbles(x);
  return x;
}

uint64_t invRound(uint64_t x, uint64_t k) { // one round of SKINNY
  x = invMixColumns(x);
  x = invShiftCells(x);
  x ^= uint64_t(2) << 8; // round constant (other part is masked by random key)
  x ^= k;
  x = invSubNibbles(x);
  return x;
}

uint64_t invLastRound(uint64_t x, uint64_t k) { // one round of SKINNY
  x = invSubNibbles(x);
  return x;
}

uint64_t FirstRound(uint64_t x, uint64_t k) { // round of SKINNY without SC (assume was performed before)
  x = SubNibbles(x);
  x ^= k;
  x ^= uint64_t(2) << (8+32); // round constant (other part is masked by random key)
  return MixColumns(x);
}

static const int PT[16] = {6,10,0,1,15,11,4,5,2,3,8,9,7,14,12,13};

uint64_t updateKey0(uint64_t key) {
  uint64_t res = 0;
  for (unsigned i = 0; i < 16; ++i) res |= ((key >> 4*PT[i]) & 0xF) << 4*i;
  return res;
}

uint64_t LFSR2_nibble(uint64_t k) {
  return ((k << 1) & 0xF) ^ (((k >> 2) ^ (k >>3)) & 1);
}

uint64_t updateKey1(uint64_t key) {
  uint64_t res = 0;
  for (unsigned i = 0; i < 16; ++i) {
    if (i%4 <= 1) res |= LFSR2_nibble(key >> 4*PT[i]) << 4*i;
    else res |= ((key >> 4*PT[i]) & 0xF) << 4*i;
  }
  return res;
}

void print64(uint64_t x) {
  for (int i = 0; i < 16; ++i) cout << ((x >> 4*i) & 0xF) << " ";
  cout << endl;
}

void test(unsigned nRun, unsigned nCores) {
  std::random_device rd;
  std::default_random_engine generator(rd());
  std::uniform_int_distribution<uint64_t> distribution(0,~uint64_t(0));

  uint64_t ntrials = 0;

  for (unsigned iRun = 0; iRun < nRun; ++iRun) {

    uint64_t k00[14];
    uint64_t k0[14];
    k00[0] = distribution(generator);
    for (unsigned i = 1; i < 14; ++i) k00[i] = updateKey0(k00[i-1]);
    for (unsigned i = 0; i < 14; ++i) k0[i] = k00[i] & 0x00FF00FF00FF00FFULL;

    uint64_t k10[14];
    uint64_t k1[14];
    k10[0] = k00[0] ^ (uint64_t(2));
    for (unsigned i = 1; i < 14; ++i) k10[i] = updateKey0(k10[i-1]);
    for (unsigned i = 0; i < 14; ++i) k1[i] = k10[i]  & 0x00FF00FF00FF00FFULL;

    uint64_t k20[14];
    uint64_t k2[14];
    k20[0] = k00[0] ^ (uint64_t(1) << 7*4);

    for (unsigned i = 1; i < 14; ++i) k20[i] = updateKey0(k20[i-1]);
    for (unsigned i = 0; i < 14; ++i) k2[i] = k20[i] & 0x00FF00FF00FF00FFULL;

    uint64_t k30[14];
    uint64_t k3[14];

    k30[0] = k10[0] ^  (uint64_t(1) << 7*4);
    for (unsigned i = 1; i < 14; ++i) k30[i] = updateKey0(k30[i-1]);
    for (unsigned i = 0; i < 14; ++i) k3[i] = k30[i]   & 0x00FF00FF00FF00FFULL;

    uint64_t const & input_diff = uint64_t(0x1001001004ULL);
    uint64_t const & output_diff = uint64_t(0x5505U) << 12*4;


    vector<uint64_t> cpt (nCores, 0);

    bool found = false;

    #pragma omp parallel for
    for (uint64_t t = 0; t < nCores; ++t) { // number of cores
      uint64_t x0 = 0;
      #pragma omp critical
      {x0 = distribution(generator);} // random input
      while (!found) {
        #pragma omp flush(found)
        cpt[t] += 1;
        x0 = Round(x0, cpt[t]); // fast local random
        uint64_t x1 = x0 ^ input_diff;
        for (unsigned r = 0; r < 13; ++r) {
          x0 = Round(x0, k0[r]);
          x1 = Round(x1, k1[r]);
        }
        x0 = LastRound(x0, k0[13]);
        uint64_t x2 = x0 ^ output_diff;
        x2 = invLastRound(x2, k2[13]);
        x1 = LastRound(x1, k1[13]);
        uint64_t x3 = x1 ^ output_diff;
        x3 = invLastRound(x3, k3[13]);

        for (unsigned r = 0; r < 13; ++r) {
          x2 = invRound(x2, k2[12-r]);
          x3 = invRound(x3, k3[12-r]);
        }
        if ((x2 ^ x3) == input_diff) {
          #pragma omp critical
          {found = true;}
        }
      }
    }
    for (auto c : cpt) ntrials += c;
  }
  ntrials /= nRun;
  cout << "number of trials: " << ntrials << endl;

}



int main(int argc, char const *argv[]) {

  test(5, 128);

  return 0;
}
